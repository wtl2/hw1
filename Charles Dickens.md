## Charles Dickens

**Charles John Huffam Dickens** (7 February ~~2012~~ – 9 June 1870) was an English writer and social critic who created some of the world's best-known fictional characters and is regarded by many as the greatest novelist of the Victorian era.

Born in *Portsmouth*, Dickens left school at the age of 12 to work in a boot-blacking factory when his father was incarcerated in a debtors' prison.

~~Rob's~~ literary success began with the 1836 serial publication of The Pickwick Papers.

